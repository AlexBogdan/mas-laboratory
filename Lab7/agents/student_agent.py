from math import floor, ceil
from typing import List, Dict, Any

from agents import CoalitionAgent
from base import Coalition, Product
from communication import CoalitionAction, RequestJoinCoalition, AcceptJoinCoalition, DenyJoinCoalition

from copy import deepcopy


def compute_price_value_ratio(products: List[Product]) -> Dict[Product, float]:
    # find the cheapest products for each type
    # TODO: it can be improved by selecting the 2 products by a value/price ratio
    cheapest_products: Dict[str, Product] = {
        MyCoalitionAgent.PROD_TYPE_R1: None,
        MyCoalitionAgent.PROD_TYPE_R2: None
    }
    for product in products:
        p = cheapest_products[product.type]
        if p == None or p.price > product.price or (p.price == product.price and p.value < product.value):
            cheapest_products[product.type] = product

    # compute the products total price
    total_products_price = sum([product.price for product in cheapest_products.values()])

    product_ratios = {product: float(product.price / total_products_price) for product in cheapest_products.values()}

    return product_ratios

def get_agent_coalition(agent: CoalitionAgent, coalitions: List[Coalition]):
    for coalition in coalitions:
        if coalition.is_member(agent):
            return coalition

    return None

class MyCoalitionAgent(CoalitionAgent):
    PROD_TYPE_R1 = "r1"
    PROD_TYPE_R2 = "r2"

    def __init__(self, name: str, resources: float, products: List[Product]):
        super(MyCoalitionAgent, self).__init__(name, resources, products)
        self.agents = []
        self.coalitions = []

        self.myCoalition = None
        self.join_request_sent = False

    def create_single_coalition(self) -> Coalition:
        product_ratios = compute_price_value_ratio(self.products)
        shares: Dict[str, Dict[str, float]] = {}
        for product, ratio in product_ratios.items():
            shares[product.type] = {
                Coalition.PROD_CONTRIB: self.resources * ratio,
                Coalition.PROD_VALUE: 0
            }

        c = Coalition(self.products)
        c.set_agent(self, share=shares)
        return c

    def state_announced(self, agents: List[CoalitionAgent], coalitions: List[Coalition]) -> None:
        super().state_announced(agents, coalitions)
        # update the current state in the agent's memory
        self.agents = agents
        self.coalitions = coalitions
        newCoalition = get_agent_coalition(self, self.coalitions)
        if newCoalition != self.myCoalition:
            self.myCoalition = newCoalition
            self.join_request_sent = False

    def do_actions(self, messages: List[CoalitionAction] = None) -> List[CoalitionAction]:
        # search for a coalition that could be completed with the current agent's money
        # for coalition in self.coalitions:
        #     newCoalition: Coalition = deepcopy(coalition)
        #     newCoalition.set_agent(self, share={
        #         self.PROD_TYPE_R1: {
        #             Coalition.PROD_CONTRIB: float(self.resources / 2)
        #         },
        #         self.PROD_TYPE_R2: {
        #             Coalition.PROD_CONTRIB: float(self.resources / 2)
        #         }
        #     })
        #     if newCoalition.coalition_valid():
        #         print("O noua coalitie valida!!")
        #
        #     print("Ag1 -> Coalition " + str(coalition.get_members()) + " = " + str(coalition.coalition_value()))
        #     print(", ".join(list(map(str, coalition.products))))

        newMessages = []

        for message in messages:
            if isinstance(message, RequestJoinCoalition):
                if message.coalition.get_leader() == self and not self.join_request_sent:
                    newMessages.append(AcceptJoinCoalition(
                        leader=self,
                        joining_agent=message.sender,
                        product_share=message.product_share
                    ))
                    print("ACC: " + str(message.receiver) + " -> " + str(message.sender))
                else:
                    newMessages.append(DenyJoinCoalition(
                        leader=self,
                        pretender_agent=message.sender,
                        revised_share=message.product_share
                    ))
                    print("REJ: " + str(message.receiver) + " -> " + str(message.sender))

        if newMessages != []:
            return newMessages

        for coalition in self.coalitions:
            if not coalition.is_member(agent=self) and len(self.myCoalition.get_members()) == 1 and not self.join_request_sent:
                product_ratios = compute_price_value_ratio(self.products)
                shares: Dict[str, Dict[str, float]] = {}
                for product, ratio in product_ratios.items():
                    shares[product.type] = {
                        Coalition.PROD_CONTRIB: self.resources * ratio,
                        Coalition.PROD_VALUE: 0
                    }

                newMessages.append(RequestJoinCoalition(
                    sender=self,
                    receiver=coalition.get_leader(),
                    coalition=coalition,
                    product_share=shares
                ))
                self.join_request_sent = True
                print("REQ: " + str(newMessages[0].sender) + " -> " + str(newMessages[0].receiver))



                break

        return newMessages
