import numpy as np

from environment import *
from random import choice, randint, random

num_episodes = 100
gamma = .9
min_eps = 1e-3
max_eps = .9
crt_eps = max_eps
learning_rate = .1
policy = {}

def eps_greedy(Q, state, actions):
    unexplored = list(filter(lambda action: not (state, action) in Q, actions))
    if len(unexplored) != 0:
        return choice(unexplored)

    if random() < crt_eps:
        return choice(actions) #actions[randint(0, len(actions) - 1)]

    max_ = -999999999
    max_action = None
    for action in actions:
        if Q.get((state, action), 0) > max_:
            max_ = Q.get((state, action), 0)
            max_action = action

    return max_action

def q_learning(env, actions):
    global policy
    global crt_eps

    Q = {}
    states = set()
    report_freq = 10
    max_steps = 1000

    d_eps = (max_eps - min_eps) / num_episodes

    for episode in range(num_episodes):
        if episode % report_freq == 0:
            print("===== Starting episode {} =====".format(episode))

        env.reset()
        done = False
        cnt = -1
        while not done:
            cnt += 1
            if cnt > max_steps:
                break

            state = env.get_world_state()
            action = eps_greedy(Q, state, actions)
            states.add(state)

            next_state, reward, done = env.step(action)

            max_ = -999999999
            for next_action in actions:
                if Q.get((next_state, next_action), 0) > max_:
                    max_ = Q.get((next_state, next_action), 0)
            Q[(state, action)] = Q.get((state, action), 0) + learning_rate * (reward + gamma * max_ - Q.get((state, action), 0))

        crt_eps -= d_eps

    for state in states:
        all_actions = [(a, b, c) for ((a, b), c) in Q.items() if a == state]
        max_reward_tuple = max(all_actions, key = lambda z : z[2])
        policy[state] = max_reward_tuple[1]

def sarsa(env, actions):
    global policy
    global crt_eps

    Q = {}
    states = set()
    report_freq = 10
    max_steps = 1000

    d_eps = (max_eps - min_eps) / num_episodes

    for episode in range(num_episodes):
        if episode % report_freq == 0:
            print("===== Starting episode {} =====".format(episode))

        env.reset()
        done = False
        cnt = -1
        state = env.get_world_state()
        action = eps_greedy(Q, state, actions)
        while not done:
            cnt += 1
            if cnt > max_steps:
                break

            states.add(state)
            next_state, reward, done = env.step(action)
            next_action = eps_greedy(Q, next_state, actions)
            Q[(state, action)] = Q.get((state, action), 0) + learning_rate * (reward + gamma * Q.get((next_state, next_action), 0) - Q.get((state, action), 0))
            state = next_state
            action = next_action

        crt_eps -= d_eps

    for state in states:
        all_actions = [(a, b, c) for ((a, b), c) in Q.items() if a == state]
        max_reward_tuple = max(all_actions, key = lambda z : z[2])
        policy[state] = max_reward_tuple[1]

if __name__ == "__main__":
    TEST_SUITE = "tests/0c/"

    EXT = ".txt"
    SI = "si"
    SF = "sf"

    start_stream = open(TEST_SUITE + SI + EXT)
    end_stream = open(TEST_SUITE + SF + EXT)

    env = DynamicEnvironment(BlocksWorld(input_stream=start_stream), BlocksWorld(input_stream=end_stream), dynamics=.0)

    available_actions_a = [NoAction(),
                            Unstack(Block("A"), Block("B")),
                            PutDown(Block("A")),
                            PickUp(Block("B")),
                            Stack(Block("B"), Block("C"))]

    available_actions_b = [NoAction(),
                            Unstack(Block("A"), Block("B")),
                            PutDown(Block("A")),
                            Unstack(Block("B"), Block("C")),
                            PutDown(Block("B")),
                            PickUp(Block("B")),
                            PickUp(Block("C")),
                            Stack(Block("B"), Block("A")),
                            Stack(Block("C"), Block("B"))]

    available_actions_c = [NoAction(),
                            Unstack(Block("A"), Block("B")),
                            PutDown(Block("A")),
                            Unstack(Block("B"), Block("C")),
                            PutDown(Block("B")),
                            Unstack(Block("C"), Block("D")),
                            PutDown(Block("C")),
                            PickUp(Block("B")),
                            PickUp(Block("C")),
                            Stack(Block("B"), Block("D")),
                            Stack(Block("C"), Block("A"))]

    #q_learning(env, available_actions_b)
    sarsa(env, available_actions_c)

    env.reset()

    state = None
    num_steps = 0

    for i in range(100):
        num_steps += 1
        print("=============== STEP %i ===============" % i)

        #act = choice(available_actions)
        if state == None:
            state = env.get_world_state()
        act = policy[state]
        next_state, reward, done = env.step(act)

        print("## Chosen action: " + str(act))
        print("## State\n")
        print(state)

        print(env)

        print("\n")
        print("Reward: " + str(reward))

        print("\n")
        print("done: " + str(done))

        state = next_state

        if done:
            break

    for state, action in policy.items():
        print("State {} -> Action {}".format(state, action))

    print("Finished in {} steps".format(num_steps))
