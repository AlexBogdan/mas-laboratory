from preflibtools import io
from preflibtools.generate_profiles import gen_mallows, gen_cand_map, gen_impartial_culture_strict
from typing import List, Dict, Tuple
import random
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import datetime

import math


PHIS = [0.7, 0.8, 0.9, 1.0]
NUM_VOTERS = [100, 500, 1000]
NUM_CANDIDATES = [3, 6, 10, 15]


def generate_random_mixture(nvoters: int = 100, ncandidates: int = 6, num_refs: int = 3, phi: float = 1.0) \
    -> Tuple[Dict[int, str], List[Dict[int, int]], List[int]]:
    """
    Function that will generate a `voting profile` where there are num_refs mixtures of a
    Mallows model, each with the same phi hyperparameter
    :param nvoters: number of voters
    :param ncandidates: number of candidates
    :param num_refs: number of Mallows Mixtures in the voting profile
    :param phi: hyper-parameter for each individual Mallows model
    :return: a tuple consisting of:
        the candidate map (map from candidate id to candidate name),
        a ranking list (list consisting of dictionaries that map from candidate id to order of preference)
        a ranking count (the number of times each vote order comes up in the ranking list,
        i.e. one or more voters may end up having the same preference over candidates)
    """
    candidate_map = gen_cand_map(ncandidates)

    mix = []
    phis = []
    refs = []

    for i in range(num_refs):
        refm, refc = gen_impartial_culture_strict(1, candidate_map)
        refs.append(io.rankmap_to_order(refm[0]))
        phis.append(phi)
        mix.append(random.randint(1,100))

    smix = sum(mix)
    mix = [float(m)/float(smix) for m in mix]

    rmaps, rmapscounts = gen_mallows(nvoters, candidate_map, mix, phis, refs)

    return candidate_map, rmaps, rmapscounts


def find_weakest_candidate(candidates_ballots) -> int:
    min_candidate = list(candidates_ballots.keys())[0]
    min_votes = len(candidates_ballots[min_candidate])

    for candidate in candidates_ballots:
        if len(candidates_ballots[candidate]) < min_votes:
            min_votes = len(candidates_ballots[candidate])
            min_candidate = candidate

    return min_candidate

def get_next_preferred_candidate(candidate: int, ballot, candidates: List[int]) -> int:
    candidate_position = ballot[candidate]
    for (key, value) in ballot.items():
        if key in candidates:
            if value > candidate_position:
                return key

    # if the ballot is exhausted, then I'll return -1
    return -1

def stv(nvoters: int,
        candidate_map: Dict[int, str],
        rankings: List[Dict[int, int]],
        ranking_counts: List,
        top_k: int,
        required_elected: int) -> List[int]:
    """
    :param nvoters: number of voters
    :param canidate_map: the mapping of candidate IDs to candidate names
    :param rankings: the expressed full rankings of voters, specified as a list of mapping from candidate_id -> rank
    :param ranking_counts:
    :param top_k: the number of preferences taken into account [min: 2, max: (num_candidates - 1), aka full STV]
    :return: The list of elected candidate id-s
    """

    # modify the ballots to consider only the first k options
    new_rankings = []
    for ballot in rankings:
        new_ballot = {}
        for key, value in ballot.items():
            if value <= top_k:
                new_ballot[key] = value
        new_rankings.append(new_ballot)

    rankings = new_rankings

    # print("Nvoters: " + str(nvoters))
    # print("Candidates: " + str(candidate_map))
    # print("Rankings: " + str(rankings))
    # print("Rankings_counts: " + str(ranking_counts))
    # print("Top_K: " + str(top_k))
    # print("Required_Elected: " + str(required_elected))
    # print()

    # print(len(rankings))

    # we'll need the 'quota' limit so we could see who will reach it first
    votes_needed_to_win: int = math.floor(len(rankings) / (required_elected + 1))
    # print("Votes_needed_to_win: " + str(votes_needed_to_win))

    # saving for each candidate the ballots in which they appear as first choice
    candidate_ballots = {candidate_id: [] for candidate_id in candidate_map}
    for ballot in rankings:
        # extract the first choice from every ballot
        first_candidate_id = list(ballot.keys())[0]
        candidate_ballots[first_candidate_id].append(ballot)

    winners: List[int] = []
    while len(winners) < required_elected < len(list(candidate_ballots.keys())):
        first_choice_count = {id: len(candidate_ballots[id]) for id in candidate_ballots}
        # print("First choices: " + str(first_choice_count))

        # we'll check if there's any candidate with more votes than the required quota
        exceded_quota = False
        for candidate in candidate_ballots:
            number_of_votes = len(candidate_ballots[candidate])
            # we've found a new winner
            if number_of_votes >= votes_needed_to_win:
                if candidate not in winners:
                    winners.append(candidate)

            # if there's a surplus, then we will transfer it to other candidates
            # based on the second choice on his ballots
            if number_of_votes > votes_needed_to_win:
                exceded_quota = True
                surplus_votes = number_of_votes - votes_needed_to_win

                for second_candidate in candidate_ballots:
                    if second_candidate is not candidate:
                        # transfer some of the votes proportionally, considering the second option
                        second_choice_ballots = [
                            ballot for ballot in candidate_ballots[candidate]
                            if get_next_preferred_candidate(candidate, ballot, candidates=list(candidate_ballots.keys())) == second_candidate
                        ]
                        transfered_votes = int(float(len(second_choice_ballots) / number_of_votes) * surplus_votes)
                        transfered_ballots = second_choice_ballots[:transfered_votes]
                        candidate_ballots[second_candidate] += transfered_ballots
                        candidate_ballots[candidate] = [ballot for ballot in candidate_ballots[candidate] if ballot not in transfered_ballots]

        #         remove any surplus that wasn't transferred
                candidate_ballots[candidate] = candidate_ballots[candidate][:votes_needed_to_win]

        # print(votes_needed_to_win)
        if not exceded_quota:
            weak_candidate = find_weakest_candidate(candidate_ballots)
            # print("Weakest candidate: " + str(weak_candidate))
            # transfer the ballots
            for ballot in candidate_ballots[weak_candidate]:
                next_choice = get_next_preferred_candidate(weak_candidate, ballot, candidates=list(candidate_ballots.keys()))
                if next_choice != -1:
                    candidate_ballots[next_choice].append(ballot)

            del candidate_ballots[weak_candidate]
            # print(candidate_ballots.keys())

    if required_elected == len(list(candidate_ballots.keys())):
        winners = list(candidate_ballots.keys())
    else:
        winners = winners[:required_elected]
    # print("Winners: " + str(winners))

    return winners

def candidates_variation_graph():
    phi = 0.8
    num_voters = 100
    required_elected = 3

    stv_n = {num_candidates: [] for num_candidates in NUM_CANDIDATES}
    stv_k = {num_candidates: [] for num_candidates in NUM_CANDIDATES}

    for num_candidates in NUM_CANDIDATES:
        for step in range(1000):
            cmap, rmaps, rmapscounts = generate_random_mixture(
                nvoters=num_voters,
                ncandidates=num_candidates,
                phi=phi
            )

            rmaps = [[ballot] * count for (ballot, count) in list(zip(rmaps, rmapscounts))]
            rmaps = [ballot for candidate_ballots in rmaps for ballot in candidate_ballots]

            stv_n[num_candidates].append(stv(
                nvoters=num_voters,
                candidate_map=cmap,
                rankings=rmaps,
                ranking_counts=rmapscounts,
                top_k=num_candidates,
                required_elected=required_elected
            ))

            stv_k[num_candidates].append(stv(
                nvoters=num_voters,
                candidate_map=cmap,
                rankings=rmaps,
                ranking_counts=rmapscounts,
                top_k=int(num_candidates / 2 + 1),
                required_elected=required_elected
            ))

    results = {}

    for num_candidates in NUM_CANDIDATES:
        count_n = 0
        count_k = 0
        for (res_n, res_k) in list(zip(stv_n[num_candidates], stv_k[num_candidates])):
            count_n += 1
            if sorted(res_n) == sorted(res_k):
                count_k += 1

        results[num_candidates] = (count_n, count_k)




    candidates = list(results.keys())
    results_n = [count_n for (count_n, count_k) in results.values()]
    results_k = [count_k for (count_n, count_k) in results.values()]

    ax = plt.subplot()
    ax.bar(candidates, results_n, color='b', align='center')
    ax.bar(candidates, results_k, color='r', align='center')
    ax.legend(['stv_n', 'stv_k'])

    plt.xlabel('Candidates Number')
    plt.ylabel('"Correct" vote results')
    plt.title('PHI = ' + str(phi) + " | VOTERS = " + str(num_voters) + " | CANDIDATES = " + str(candidates) + " | ELECTED = " + str(required_elected))

    plt.show()


def voters_variation_graph():
    phi = 0.8
    num_candidates = 6
    required_elected = 3

    stv_n = {num_voters: [] for num_voters in NUM_VOTERS}
    stv_k = {num_voters: [] for num_voters in NUM_VOTERS}

    for num_voters in NUM_VOTERS:
        for step in range(1000):
            cmap, rmaps, rmapscounts = generate_random_mixture(
                nvoters=num_voters,
                ncandidates=num_candidates,
                phi=phi
            )

            rmaps = [[ballot] * count for (ballot, count) in list(zip(rmaps, rmapscounts))]
            rmaps = [ballot for candidate_ballots in rmaps for ballot in candidate_ballots]

            stv_n[num_voters].append(stv(
                nvoters=num_voters,
                candidate_map=cmap,
                rankings=rmaps,
                ranking_counts=rmapscounts,
                top_k=num_candidates,
                required_elected=required_elected
            ))

            stv_k[num_voters].append(stv(
                nvoters=num_voters,
                candidate_map=cmap,
                rankings=rmaps,
                ranking_counts=rmapscounts,
                top_k=int(num_candidates / 2 + 1),
                required_elected=required_elected
            ))

    results = {}

    for num_voters in NUM_VOTERS:
        count_n = 0
        count_k = 0
        for (res_n, res_k) in list(zip(stv_n[num_voters], stv_k[num_voters])):
            count_n += 1
            if sorted(res_n) == sorted(res_k):
                count_k += 1

        results[num_voters] = (count_n, count_k)

    voters = list(map(lambda x: x/100, list(results.keys())))
    results_n = [count_n for (count_n, count_k) in results.values()]
    results_k = [count_k for (count_n, count_k) in results.values()]

    ax = plt.subplot()
    ax.bar(voters, results_n, color='b', align='center')
    ax.bar(voters, results_k, color='r', align='center')
    ax.legend(['stv_n', 'stv_k'])

    plt.xlabel('Voters Number (x100)')
    plt.ylabel('"Correct" vote results')
    plt.title('PHI = ' + str(phi) + " | VOTERS = " + str(voters) + " | CANDIDATES = " + str(
        num_candidates) + " | ELECTED = " + str(required_elected))

    plt.show()

def phis_variation_graph():
    num_voters = 500
    num_candidates = 6
    required_elected = 3

    stv_n = {phi: [] for phi in PHIS}
    stv_k = {phi: [] for phi in PHIS}

    for phi in PHIS:
        for step in range(1000):
            cmap, rmaps, rmapscounts = generate_random_mixture(
                nvoters=num_voters,
                ncandidates=num_candidates,
                phi=phi
            )

            rmaps = [[ballot] * count for (ballot, count) in list(zip(rmaps, rmapscounts))]
            rmaps = [ballot for candidate_ballots in rmaps for ballot in candidate_ballots]

            stv_n[phi].append(stv(
                nvoters=num_voters,
                candidate_map=cmap,
                rankings=rmaps,
                ranking_counts=rmapscounts,
                top_k=num_candidates,
                required_elected=required_elected
            ))

            stv_k[phi].append(stv(
                nvoters=num_voters,
                candidate_map=cmap,
                rankings=rmaps,
                ranking_counts=rmapscounts,
                top_k=int(num_candidates / 2 + 1),
                required_elected=required_elected
            ))

    results = {}

    for phi in PHIS:
        count_n = 0
        count_k = 0
        for (res_n, res_k) in list(zip(stv_n[phi], stv_k[phi])):
            count_n += 1
            if sorted(res_n) == sorted(res_k):
                count_k += 1

        results[phi] = (count_n, count_k)

    phis = list(results.keys())
    results_n = [count_n for (count_n, count_k) in results.values()]
    results_k = [count_k for (count_n, count_k) in results.values()]

    ax = plt.subplot()
    ax.bar(phis, results_n, width=0.05, color='b', align='center')
    ax.bar(phis, results_k, width=0.05, color='r', align='center')
    ax.legend(['stv_n', 'stv_k'])

    plt.xlabel('Phi')
    plt.ylabel('"Correct" vote results')
    plt.title('PHI = ' + str(phis) + " | VOTERS = " + str(num_voters) + " | CANDIDATES = " + str(
        num_candidates) + " | ELECTED = " + str(required_elected))

    plt.show()

if __name__ == "__main__":
    PHIS = [0.7, 0.8, 0.9, 1.0]
    NUM_VOTERS = [100, 500, 1000]
    NUM_CANDIDATES = [3, 6, 10, 15]

    # candidates_variation_graph()
    # voters_variation_graph()
    phis_variation_graph()

    # for phi in PHIS:
    #     for num_voters in NUM_VOTERS:
    #         for num_candidates in NUM_CANDIDATES:
    #             cmap, rmaps, rmapscounts = generate_random_mixture(
    #                 nvoters=num_voters,
    #                 ncandidates=num_candidates,
    #                 phi=phi
    #             )
    #
    #             rmaps = [[ballot] * count for (ballot, count) in list(zip(rmaps, rmapscounts))]
    #             rmaps = [ballot for candidate_ballots in rmaps for ballot in candidate_ballots]
    #
    #             stv(
    #                 nvoters=num_voters,
    #                 candidate_map=cmap,
    #                 rankings=rmaps,
    #                 ranking_counts=rmapscounts,
    #                 top_k=6,
    #                 required_elected=3
    #             )
    #             # exit(1)
