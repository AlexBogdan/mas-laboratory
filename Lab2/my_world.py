from base import Agent, Action, Perception
from representation import GridRelativeOrientation, GridOrientation, GridPosition
from hunting import HuntingEnvironment, WildLifeAgentData, WildLifeAgent
from enum import Enum
from communication import *

from collections import deque

import time, random

class ProbabilityMap(object):

    def __init__(self, existing_map = None):
        self.__internal_dict = {}

        if existing_map:
            for k, v in existing_map.list_actions():
                self.__internal_dict[k] = v

    def empty(self):
        if self.__internal_dict:
            return False

        return True


    def put(self, action, value):
        self.__internal_dict[action] = value

    def remove(self, action):
        """
        Updates a discrete action probability map by uniformly redistributing the probability of an action to remove over
        the remaining possible actions in the map.
        :param action: The action to remove from the map
        :return:
        """
        if action in self.__internal_dict:
            val = self.__internal_dict[action]
            del self.__internal_dict[action]

            remaining_actions = list(self.__internal_dict.keys())
            nr_remaining_actions = len(remaining_actions)

            if nr_remaining_actions != 0:
                prob_sum = 0
                for i in range(nr_remaining_actions - 1):
                    new_action_prob = (self.__internal_dict[remaining_actions[i]] + val) / float(nr_remaining_actions)
                    prob_sum += new_action_prob

                    self.__internal_dict[remaining_actions[i]] = new_action_prob

                self.__internal_dict[remaining_actions[nr_remaining_actions - 1]] = 1 - prob_sum


    def choice(self):
        """
        Return a random action from a discrete distribution over a set of possible actions.
        :return: an action chosen from the set of choices
        """
        r = random.random()
        count_prob = 0

        for a in self.__internal_dict.keys():
            count_prob += self.__internal_dict[a]
            if count_prob >= r:
                return a

        raise RuntimeError("Should never get to this point when selecting an action")

    def list_actions(self):
        return self.__internal_dict.items()


class MyAction(Action, Enum):
    """
    Physical actions for wildlife agents.
    """

    # The agent must move north (up)
    NORTH = 0

    # The agent must move east (right).
    EAST = 1

    # The agent must move south (down).
    SOUTH = 2

    # The agent must move west (left).
    WEST = 3



class MyAgentPerception(Perception):
    """
    The perceptions of a wildlife agent.
    """

    def __init__(self, agent_position, obstacles, nearby_predators, nearby_prey, messages = None):
        """
        Default constructor
        :param agent_position: agents's position.
        :param obstacles: visible obstacles
        :param nearby_predators: visible predators - given as tuple (agent_id, grid position)
        :param nearby_prey: visible prey - given as tuple (agent_id, grid_position)
        :param messages: incoming messages, may be None
        """
        self.agent_position = agent_position
        self.obstacles = obstacles
        self.nearby_predators = nearby_predators
        self.nearby_prey = nearby_prey

        if messages:
            self.messages = messages
        else:
            self.messages = []


class MyPrey(WildLifeAgent):
    """
    Implementation of the prey agent.
    """
    UP_PROB = 0.25
    LEFT_PROB = 0.25
    RIGHT_PROB = 0.25
    DOWN_PROB = 0.25

    def __init__(self):
        super(MyPrey, self).__init__(WildLifeAgentData.PREY)


    def response(self, perceptions):
        """
        TODO: your code here
        :param perceptions: The perceptions of the agent at each step
        :return: The `Action' that your agent takes after perceiving the environment at each step
        """
        agent_pos = perceptions.agent_position
        probability_map = ProbabilityMap()
        probability_map.put(MyAction.NORTH,  MyPrey.UP_PROB)
        probability_map.put(MyAction.SOUTH, MyPrey.DOWN_PROB)
        probability_map.put(MyAction.WEST, MyPrey.LEFT_PROB)
        probability_map.put(MyAction.EAST, MyPrey.RIGHT_PROB)

        for obstacle_pos in perceptions.obstacles:
            if agent_pos.get_distance_to(obstacle_pos) > 1:
                continue

            relative_orientation = agent_pos.get_simple_relative_orientation(obstacle_pos)
            if relative_orientation == GridRelativeOrientation.FRONT:
                probability_map.remove(MyAction.NORTH)

            elif relative_orientation == GridRelativeOrientation.BACK:
                probability_map.remove(MyAction.SOUTH)

            elif relative_orientation == GridRelativeOrientation.RIGHT:
                probability_map.remove(MyAction.EAST)

            elif relative_orientation == GridRelativeOrientation.LEFT:
                probability_map.remove(MyAction.WEST)

        ## save available moves
        available_moves = ProbabilityMap(existing_map=probability_map)

        ## examine actions which are unavailable because of predators
        for (_, predator_pos) in perceptions.nearby_predators:
            relative_pos = agent_pos.get_simple_relative_orientation(predator_pos)

            if relative_pos == GridRelativeOrientation.FRONT:
                probability_map.remove(MyAction.NORTH)

            elif relative_pos == GridRelativeOrientation.FRONT_LEFT:
                probability_map.remove(MyAction.NORTH)
                probability_map.remove(MyAction.WEST)

            elif relative_pos == GridRelativeOrientation.FRONT_RIGHT:
                probability_map.remove(MyAction.NORTH)
                probability_map.remove(MyAction.EAST)

            elif relative_pos == GridRelativeOrientation.LEFT:
                probability_map.remove(MyAction.WEST)

            elif relative_pos == GridRelativeOrientation.RIGHT:
                probability_map.remove(MyAction.EAST)

            elif relative_pos == GridRelativeOrientation.BACK:
                probability_map.remove(MyAction.SOUTH)

            elif relative_pos == GridRelativeOrientation.BACK_LEFT:
                probability_map.remove(MyAction.SOUTH)
                probability_map.remove(MyAction.WEST)

            elif relative_pos == GridRelativeOrientation.BACK_RIGHT:
                probability_map.remove(MyAction.SOUTH)
                probability_map.remove(MyAction.EAST)

        if not probability_map.empty():
            return probability_map.choice()
        else:
            return available_moves.choice()



class MyPredator(WildLifeAgent):

    def __init__(self, mapWidth, mapHeight):
        super(MyPredator, self).__init__(WildLifeAgentData.PREDATOR)

        # each predator will get a part of the map that needs to be stalked
        # we will decide this area after we will meet our teammates
        self.min_area_w = 1
        self.min_area_h = 1
        self.max_area_w = mapWidth
        self.max_area_h = mapHeight

        self.is_area_established = False
        self.engaged_in_hunt = False
        self.going_to_help = False

        # We need to know where the map center is so we can meet our teammates there
        self.center: GridPosition = GridPosition(mapWidth / 2, mapHeight / 2)

        # How much do we need to wait for everyone to come here
        self.waiting_time = mapWidth / 2 + mapHeight / 2 - 2

        # A list of the current predator's known teammates
        self.teammates: [MyPredator] = []

        # it tells the predator where he needs to go
        self.next_target_position = deque()

    # set what area the current predator will cover
    def define_stalking_area(self):
        height = int(self.max_area_h / len(self.teammates))
        self.min_area_h = self.id * height + 1
        self.max_area_h = (self.id + 1) * height

    # How far is the center from the current position
    def distanceFromCenter(self, agentPosition: GridPosition):
        return agentPosition.get_distance_to(self.center)

    def go_to_position(self, agentPosition: GridPosition, destinationPosition: GridPosition):
        relative_pos = agentPosition.get_simple_relative_orientation(destinationPosition)

        if relative_pos == GridRelativeOrientation.FRONT:
            return MyAction.NORTH
        elif relative_pos == GridRelativeOrientation.FRONT_LEFT:
            return MyAction.NORTH
        elif relative_pos == GridRelativeOrientation.FRONT_RIGHT:
            return MyAction.NORTH
        elif relative_pos == GridRelativeOrientation.LEFT:
            return MyAction.WEST
        elif relative_pos == GridRelativeOrientation.RIGHT:
            return MyAction.EAST
        elif relative_pos == GridRelativeOrientation.BACK:
            return MyAction.SOUTH
        elif relative_pos == GridRelativeOrientation.BACK_LEFT:
            return MyAction.SOUTH
        elif relative_pos == GridRelativeOrientation.BACK_RIGHT:
            return MyAction.SOUTH

    # define a better one
    def reset_patrolling_path(self):
        self.next_target_position.clear()

        self.next_target_position.append(GridPosition(self.min_area_w, self.min_area_h))
        self.next_target_position.append(GridPosition(self.max_area_w, self.max_area_h))

    def get_patrolling_direction(self, predator_position):
        if predator_position.get_distance_to(self.next_target_position[0]) == 0:
            self.next_target_position.popleft()

        if len(self.next_target_position) == 0:
            self.reset_patrolling_path()

        return SocialAction(self.go_to_position(predator_position, self.next_target_position[0]))

    def get_best_message_target(self, perceptions):
        predator_position = perceptions.agent_position
        # check if there are any messages for the current predator
        messages = AgentMessage.filter_messages_for(perceptions.messages, self)

        # get the closest prey to chase
        min_dist = 1000 * self.max_area_h
        best_target = None
        for message in messages:
            if predator_position.get_distance_to(message.content) < min_dist:
                min_dist = predator_position.get_distance_to(message.content)
                best_target = message.content

        return best_target

    def response(self, perceptions: MyAgentPerception):
        """
        TODO your response function for the predator agent
        :param perceptions:
        :return:
        """
        predator_position = perceptions.agent_position

        # update the known teammates list if they are near
        self.teammates = list(set(self.teammates) | set([id for (id, _) in perceptions.nearby_predators]))

        # we can stay around the center to meet other predators
        if self.waiting_time > 0:
            self.waiting_time -= 1
            # move to the center
            return SocialAction(self.go_to_position(predator_position, self.center))

        # define which area the current predator has in his duty
        if not self.is_area_established:
            self.define_stalking_area()
            self.is_area_established = True

        if self.engaged_in_hunt:
            # we have to catch the closest target always
            self.going_to_help = False

            # hunt the nearby prey if it's still alive
            if len(perceptions.nearby_prey) > 0:
                action = SocialAction(self.go_to_position(predator_position, perceptions.nearby_prey[0][1]))
                # call the predators from the neighbour areas to help
                for predator_id in self.teammates:
                    if predator_id == self.id + 1 or predator_id == self.id - 1:
                        # sending the other predators the prey's location
                        action.add_outgoing_message(self.id, predator_id, perceptions.nearby_prey[0][1])
                return action
        elif self.going_to_help:
            self.next_target_position.clear()
            # respond the call and go in that direction
            best_target = self.get_best_message_target(perceptions)
            if best_target is not None:
                return SocialAction(self.go_to_position(predator_position, best_target))
            else:
                # the other chase is over
                self.going_to_help = False
        elif len(perceptions.nearby_prey) > 0:
            # check if there's any prey around
            self.next_target_position.clear()
            self.engaged_in_hunt = True
            return SocialAction(self.go_to_position(predator_position, perceptions.nearby_prey[0][1]))
        elif self.get_best_message_target(perceptions) is not None:
            # check if someone else needs help on a chase
            self.going_to_help = True
            best_target = self.get_best_message_target(perceptions)
            return SocialAction(self.go_to_position(predator_position, best_target))

        # nothing happens, we are just patrolling
        self.going_to_help = False
        self.engaged_in_hunt = False
        if len(self.next_target_position) == 0:
            self.reset_patrolling_path()

        return self.get_patrolling_direction(predator_position)


class MyEnvironment(HuntingEnvironment):
    """
    Your implementation of the environment in which cleaner agents work.
    """
    PREY_RANGE = 2
    PREDATOR_RANGE = 3

    def __init__(self, w, h, num_predators, num_prey):
        """
        Default constructor. This should call the initialize methods offered by the super class.
        """
        rand_seed = 42
        # rand_seed =  datetime.now()

        print("Seed = %i" % rand_seed)

        super(MyEnvironment, self).__init__()

        predators = []
        prey = []

        for i in range(num_predators):
            predators.append(MyPredator(w, h))

        for i in range(num_prey):
            prey.append(MyPrey())

        """ Message box for messages that need to be delivered by the environment to their respective recepients, on
        the next turn """
        self.message_box = []

        ## initialize the huniting environment
        self.initialize(w=w, h=h, predator_agents=predators, prey_agents=prey, rand_seed = rand_seed)


    def step(self):
        """
        This method should iterate through all agents, provide them provide them with perceptions, and apply the
        action they return.
        """
        """
        STAGE 1: enerate perceptions for all agents, based on the state of the environment at the beginning of this
        turn
        """
        agent_perceptions = {}

        ## get perceptions for prey agents
        for prey_data in self._prey_agents:
            nearby_obstacles = self.get_nearby_obstacles(prey_data.grid_position, MyEnvironment.PREY_RANGE)
            nearby_predators = self.get_nearby_predators(prey_data.grid_position, MyEnvironment.PREY_RANGE)
            nearby_prey = self.get_nearby_prey(prey_data.grid_position, MyEnvironment.PREY_RANGE)

            predators = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_predators]
            prey = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_prey]

            agent_perceptions[prey_data] = MyAgentPerception(agent_position=prey_data.grid_position,
                                                             obstacles=nearby_obstacles,
                                                             nearby_predators=predators,
                                                             nearby_prey=prey)

        ## TODO: create perceptions for predator agents, including messages in the `message_box`
        ## get perceptions for predator agents
        for predator_data in self._predator_agents:
            nearby_obstacles = self.get_nearby_obstacles(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_predators = self.get_nearby_predators(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_prey = self.get_nearby_prey(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)

            predators = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_predators]
            prey = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_prey]

            agent_perceptions[predator_data] = MyAgentPerception(agent_position=predator_data.grid_position,
                                                             obstacles=nearby_obstacles,
                                                             nearby_predators=predators,
                                                             nearby_prey=prey,
                                                             messages=self.message_box)


        """
        STAGE 2: call response for each agent to obtain desired actions
        """
        agent_actions = {}

        for prey_data in self._prey_agents:
            agent_actions[prey_data] = prey_data.linked_agent.response(agent_perceptions[prey_data])

        ## TODO: get actions for all agents
        for predator_data in self._predator_agents:
            agent_actions[predator_data] = predator_data.linked_agent.response(agent_perceptions[predator_data])


        """
        STAGE 3: apply the agents' actions in the environment
        """
        for prey_data in self._prey_agents:
            if not prey_data in agent_actions:
                # print("Agent %s did not opt for any action!" % str(prey_data))
                pass
            else:
                prey_action = agent_actions[prey_data]
                new_position = None

                if prey_action == MyAction.NORTH:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.NORTH)
                elif prey_action == MyAction.SOUTH:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.SOUTH)
                elif prey_action == MyAction.EAST:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.EAST)
                elif prey_action == MyAction.WEST:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.WEST)

                if not new_position in self._xtiles:
                    prey_data.grid_position = new_position
                # else:
                #     print("Agent %s tried to go through a wall!" % str(prey_data))

        # remove the old messages
        self.message_box.clear()

        for predator_data in self._predator_agents:
            if not predator_data in agent_actions:
            #     print("Agent %s did not opt for any action!" % str(predator_data))
                pass
            else:
                predator_action = agent_actions[predator_data].action
                new_position = None
                ## TODO: handle case for a SocialAction instance
                self.message_box += agent_actions[predator_data].outgoing_messages

                if predator_action == MyAction.NORTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.NORTH)
                elif predator_action == MyAction.SOUTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.SOUTH)
                elif predator_action == MyAction.EAST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.EAST)
                elif predator_action == MyAction.WEST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.WEST)

                if not new_position in self._xtiles:
                    predator_data.grid_position = new_position
                # else:
                #     print("Agent %s tried to go through a wall!" % str(predator_data))


        """
        At the end of the turn remove the dead prey
        """
        self.remove_dead_prey()


class Tester(object):

    NUM_PREDATORS = 3
    NUM_PREY = 10

    WIDTH = 30
    HEIGHT = 10

    DELAY = 0.1

    def __init__(self):
        self.env = MyEnvironment(Tester.WIDTH, Tester.HEIGHT, Tester.NUM_PREDATORS, Tester.NUM_PREY)
        self.make_steps()


    def make_steps(self):
        while not self.env.goals_completed():
            self.env.step()

            print(self.env)

            time.sleep(Tester.DELAY)

            # return



if __name__ == "__main__":
    tester = Tester()
    tester.make_steps()

