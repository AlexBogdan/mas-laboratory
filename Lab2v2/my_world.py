from base import Agent, Action, Perception
from representation import GridRelativeOrientation, GridOrientation, GridPosition
from hunting import HuntingEnvironment, WildLifeAgentData, WildLifeAgent
from communication import AgentMessage, SocialAction, CustomMessage
from enum import Enum

import time, random

import numpy as np

class ProbabilityMap(object):

    def __init__(self, existing_map = None):
        self.__internal_dict = {}

        if existing_map:
            for k, v in existing_map.list_actions():
                self.__internal_dict[k] = v

    def empty(self):
        if self.__internal_dict:
            return False

        return True


    def put(self, action, value):
        self.__internal_dict[action] = value

    def get(self, action, value):
        return self.__internal_dict.get(action, 0)

    def update_prob(self, action, value):
        if action in self.__internal_dict:
            self.__internal_dict[action] += value

    def normalize_probs(self):
        values = [] #list(self.__internal_dict.values())
        for k in self.__internal_dict:
            values.append(self.__internal_dict[k])
        min_ = min(values)

        if min_ < 0.0:
            values = list(map(lambda x : x + min_, values))

        values = values / np.sum(values)

        idx = 0
        for k in self.__internal_dict:
            self.__internal_dict[k] = values[idx]
            idx += 1

    def print_(self):
        for k in self.__internal_dict:
            print("Action: " + str(k) + " | Prob: " + str(self.__internal_dict[k]))

    def remove(self, action):
        """
        Updates a discrete action probability map by uniformly redistributing the probability of an action to remove over
        the remaining possible actions in the map.
        :param action: The action to remove from the map
        :return:
        """
        if action in self.__internal_dict:
            val = self.__internal_dict[action]
            del self.__internal_dict[action]

            remaining_actions = list(self.__internal_dict.keys())
            nr_remaining_actions = len(remaining_actions)

            if nr_remaining_actions != 0:
                prob_sum = 0
                for i in range(nr_remaining_actions - 1):
                    new_action_prob = (self.__internal_dict[remaining_actions[i]] + val) / float(nr_remaining_actions)
                    prob_sum += new_action_prob

                    self.__internal_dict[remaining_actions[i]] = new_action_prob

                self.__internal_dict[remaining_actions[nr_remaining_actions - 1]] = 1 - prob_sum


    def choice(self):
        """
        Return a random action from a discrete distribution over a set of possible actions.
        :return: an action chosen from the set of choices
        """
        r = random.random()
        count_prob = 0

        for a in self.__internal_dict.keys():
            count_prob += self.__internal_dict[a]
            if count_prob >= r:
                return a

        raise RuntimeError("Should never get to this point when selecting an action")

    def list_actions(self):
        return self.__internal_dict.items()


class MyAction(Action, Enum):
    """
    Physical actions for wildlife agents.
    """

    # The agent must move north (up)
    NORTH = 0

    # The agent must move east (right).
    EAST = 1

    # The agent must move south (down).
    SOUTH = 2

    # The agent must move west (left).
    WEST = 3

    STAY = 4



class MyAgentPerception(Perception):
    """
    The perceptions of a wildlife agent.
    """

    def __init__(self, agent_position, obstacles, nearby_predators, nearby_prey, messages = None):
        """
        Default constructor
        :param agent_position: agents's position.
        :param obstacles: visible obstacles
        :param nearby_predators: visible predators - given as tuple (agent_id, grid position)
        :param nearby_prey: visible prey - given as tuple (agent_id, grid_position)
        :param messages: incoming messages, may be None
        """
        self.agent_position = agent_position
        self.obstacles = obstacles
        self.nearby_predators = nearby_predators
        self.nearby_prey = nearby_prey

        if messages:
            self.messages = messages
        else:
            self.messages = []


class MyPrey(WildLifeAgent):
    """
    Implementation of the prey agent.
    """
    UP_PROB = 0.25
    LEFT_PROB = 0.25
    RIGHT_PROB = 0.25
    DOWN_PROB = 0.25

    def __init__(self):
        super(MyPrey, self).__init__(WildLifeAgentData.PREY)


    def response(self, perceptions):
        """
        TODO: your code here
        :param perceptions: The perceptions of the agent at each step
        :return: The `Action' that your agent takes after perceiving the environment at each step
        """
        agent_pos = perceptions.agent_position
        msgs = perceptions.messages


        probability_map = ProbabilityMap()
        probability_map.put(MyAction.NORTH,  MyPrey.UP_PROB)
        probability_map.put(MyAction.SOUTH, MyPrey.DOWN_PROB)
        probability_map.put(MyAction.WEST, MyPrey.LEFT_PROB)
        probability_map.put(MyAction.EAST, MyPrey.RIGHT_PROB)

        for obstacle_pos in perceptions.obstacles:
            if agent_pos.get_distance_to(obstacle_pos) > 1:
                continue

            relative_orientation = agent_pos.get_simple_relative_orientation(obstacle_pos)
            if relative_orientation == GridRelativeOrientation.FRONT:
                probability_map.remove(MyAction.NORTH)

            elif relative_orientation == GridRelativeOrientation.BACK:
                probability_map.remove(MyAction.SOUTH)

            elif relative_orientation == GridRelativeOrientation.RIGHT:
                probability_map.remove(MyAction.EAST)

            elif relative_orientation == GridRelativeOrientation.LEFT:
                probability_map.remove(MyAction.WEST)

        ## save available moves
        available_moves = ProbabilityMap(existing_map=probability_map)

        ## examine actions which are unavailable because of predators
        for (_, predator_pos) in perceptions.nearby_predators:
            relative_pos = agent_pos.get_simple_relative_orientation(predator_pos)

            if relative_pos == GridRelativeOrientation.FRONT:
                probability_map.remove(MyAction.NORTH)

            elif relative_pos == GridRelativeOrientation.FRONT_LEFT:
                probability_map.remove(MyAction.NORTH)
                probability_map.remove(MyAction.WEST)

            elif relative_pos == GridRelativeOrientation.FRONT_RIGHT:
                probability_map.remove(MyAction.NORTH)
                probability_map.remove(MyAction.EAST)

            elif relative_pos == GridRelativeOrientation.LEFT:
                probability_map.remove(MyAction.WEST)

            elif relative_pos == GridRelativeOrientation.RIGHT:
                probability_map.remove(MyAction.EAST)

            elif relative_pos == GridRelativeOrientation.BACK:
                probability_map.remove(MyAction.SOUTH)

            elif relative_pos == GridRelativeOrientation.BACK_LEFT:
                probability_map.remove(MyAction.SOUTH)
                probability_map.remove(MyAction.WEST)

            elif relative_pos == GridRelativeOrientation.BACK_RIGHT:
                probability_map.remove(MyAction.SOUTH)
                probability_map.remove(MyAction.EAST)

        if not probability_map.empty():
            return probability_map.choice()
        else:
            return available_moves.choice()



class MyPredator(WildLifeAgent):
    UP_PROB = 0.25
    LEFT_PROB = 0.25
    RIGHT_PROB = 0.25
    DOWN_PROB = 0.25

    ALL_ARE_PATROLING = False

    def __init__(self):
        super(MyPredator, self).__init__(WildLifeAgentData.PREDATOR)
        self.gathered = False
        self.known_predators = []
        self.patroling = False
        self.has_target = False

    def set_gather_target(self, target_col, target_row):
        self.target_cell = GridPosition(target_col, target_row)

    def set_patrol_target(self, target_col, target_row):
        self.patrol_cell = GridPosition(target_col, target_row)

    def set_current_target(self, target):
        self.current_target_cell = target
        self.has_target = True

    def unset_current_target(self):
        self.has_target = False

    def _get_action_from_orientation(self, relative_orientation):
        if relative_orientation == GridRelativeOrientation.FRONT:
            return MyAction.NORTH
        elif relative_orientation == GridRelativeOrientation.FRONT_RIGHT:
            return MyAction.NORTH
        elif relative_orientation == GridRelativeOrientation.RIGHT:
            return MyAction.EAST
        elif relative_orientation == GridRelativeOrientation.BACK_RIGHT:
            return MyAction.SOUTH
        elif relative_orientation == GridRelativeOrientation.BACK:
            return MyAction.SOUTH
        elif relative_orientation == GridRelativeOrientation.BACK_LEFT:
            return MyAction.SOUTH
        elif relative_orientation == GridRelativeOrientation.LEFT:
            return MyAction.WEST
        elif relative_orientation == GridRelativeOrientation.FRONT_LEFT:
            return MyAction.NORTH

    def _dist_manhattan(r0, c0, r1, c1):
        return abs(r0 - r1) + abs(c0 - c1)

    def remove_target(self, prey_data):
        if self.has_target and self.current_target_cell.get_distance_to(prey_data.grid_position) <= 2:
            self.unset_current_target()

    def response(self, perceptions):
        """
        TODO your response function for the predator agent
        :param perceptions:
        :return:
        """

        agent_pos = perceptions.agent_position
        msgs = perceptions.messages

        if self.has_target:
            found = False
            for msg in msgs:
                for k, v in msg.content.prey_pos.items():
                    if v.get_distance_to(self.current_target_cell) <= 2:
                        found = True
                        break
            if found:
                self.unset_current_target()
            

        if self.gathered and MyPredator.ALL_ARE_PATROLING and not self.has_target:
            # Process messages and look for the closest target
            min_dist = 999
            crt_dist = 0
            min_prey_id = -1
            min_prey_pos = None
            for msg in msgs:
                content = msg.content
                for k, v in content.prey_pos.items():
                    crt_dist = agent_pos.get_distance_to(v)
                    if crt_dist < min_dist:
                        min_dist = crt_dist
                        min_prey_id = k
                        min_prey_pos = v
            if min_prey_id != -1:
                self.set_current_target(min_prey_pos)

            for (_prey_id, _prey_pos) in perceptions.nearby_prey:
                crt_dist = agent_pos.get_distance_to(_prey_pos)
                if crt_dist < min_dist:
                    min_dist = crt_dist
                    min_prey_id = _prey_id
                    min_prey_pos = _prey_pos
                
            if min_prey_id != -1:
                self.set_current_target(min_prey_pos)
            else:
                self.unset_current_target()

        if not self.gathered:
            # Step 0 - gather in the center of the map in order to identify all other predators
            relative_orientation = agent_pos.get_simple_relative_orientation(self.target_cell)

            return self._get_action_from_orientation(relative_orientation)

        elif not self.patroling or not MyPredator.ALL_ARE_PATROLING:
            # Step 1 - move towards each specific patrol point
            relative_orientation = agent_pos.get_simple_relative_orientation(self.patrol_cell)

            return self._get_action_from_orientation(relative_orientation)
        elif self.has_target:
            # Step 2 - move towards a prey
            relative_orientation = agent_pos.get_simple_relative_orientation(self.current_target_cell)

            return self._get_action_from_orientation(relative_orientation)
        else:
            # Step 3 - no target, traverse the grid in search for a prey, avoiding other predators
            # (ensure that the map is sufficiently explored)
            prey_potential = 0.5
            predator_potential = 0.2

            probability_map = ProbabilityMap()
            probability_map.put(MyAction.NORTH,  MyPredator.UP_PROB)
            probability_map.put(MyAction.SOUTH, MyPredator.DOWN_PROB)
            probability_map.put(MyAction.WEST, MyPredator.LEFT_PROB)
            probability_map.put(MyAction.EAST, MyPredator.RIGHT_PROB)
            available_moves = ProbabilityMap(existing_map=probability_map)

            for obstacle_pos in perceptions.obstacles:
                if agent_pos.get_distance_to(obstacle_pos) > 1:
                    continue

                relative_orientation = agent_pos.get_simple_relative_orientation(obstacle_pos)
                
                if relative_orientation == GridRelativeOrientation.FRONT:
                    probability_map.remove(MyAction.NORTH)

                elif relative_orientation == GridRelativeOrientation.BACK:
                    probability_map.remove(MyAction.SOUTH)

                elif relative_orientation == GridRelativeOrientation.RIGHT:
                    probability_map.remove(MyAction.EAST)

                elif relative_orientation == GridRelativeOrientation.LEFT:
                    probability_map.remove(MyAction.WEST)
            
            ## examine actions which are less likely to be chosen because of other predators
            for (_, predator_pos) in perceptions.nearby_predators:
                relative_pos = agent_pos.get_simple_relative_orientation(predator_pos)

                if relative_pos == GridRelativeOrientation.FRONT:
                    probability_map.update_prob(MyAction.NORTH, -predator_potential)

                elif relative_pos == GridRelativeOrientation.FRONT_LEFT:
                    probability_map.update_prob(MyAction.NORTH, -predator_potential)
                    probability_map.update_prob(MyAction.WEST, -predator_potential)

                elif relative_pos == GridRelativeOrientation.FRONT_RIGHT:
                    probability_map.update_prob(MyAction.NORTH, -predator_potential)
                    probability_map.update_prob(MyAction.EAST, -predator_potential)

                elif relative_pos == GridRelativeOrientation.LEFT:
                    probability_map.update_prob(MyAction.WEST, -predator_potential)

                elif relative_pos == GridRelativeOrientation.RIGHT:
                    probability_map.update_prob(MyAction.EAST, -predator_potential)

                elif relative_pos == GridRelativeOrientation.BACK:
                    probability_map.update_prob(MyAction.SOUTH, -predator_potential)

                elif relative_pos == GridRelativeOrientation.BACK_LEFT:
                    probability_map.update_prob(MyAction.SOUTH, -predator_potential)
                    probability_map.update_prob(MyAction.WEST, -predator_potential)

                elif relative_pos == GridRelativeOrientation.BACK_RIGHT:
                    probability_map.update_prob(MyAction.SOUTH, -predator_potential)
                    probability_map.update_prob(MyAction.EAST, -predator_potential)

            probability_map.normalize_probs()

            #probability_map.print_()
            

            if not probability_map.empty():
                return probability_map.choice()
            else:
                return available_moves.choice()
            ###return probability_map.choice()
        '''
        prey_potential = 0.5
        predator_potential = 0.2

        for msg in perceptions.messages:
            msg.print_()

        agent_pos = perceptions.agent_position
        probability_map = ProbabilityMap()
        probability_map.put(MyAction.NORTH,  MyPredator.UP_PROB)
        probability_map.put(MyAction.SOUTH, MyPredator.DOWN_PROB)
        probability_map.put(MyAction.WEST, MyPredator.LEFT_PROB)
        probability_map.put(MyAction.EAST, MyPredator.RIGHT_PROB)

        for obstacle_pos in perceptions.obstacles:
            if agent_pos.get_distance_to(obstacle_pos) > 1:
                continue

            relative_orientation = agent_pos.get_simple_relative_orientation(obstacle_pos)
            
            if relative_orientation == GridRelativeOrientation.FRONT:
                probability_map.remove(MyAction.NORTH)

            elif relative_orientation == GridRelativeOrientation.BACK:
                probability_map.remove(MyAction.SOUTH)

            elif relative_orientation == GridRelativeOrientation.RIGHT:
                probability_map.remove(MyAction.EAST)

            elif relative_orientation == GridRelativeOrientation.LEFT:
                probability_map.remove(MyAction.WEST)
            

        ## save available moves
        available_moves = ProbabilityMap(existing_map=probability_map)

        ## examine actions which are less likely to be chosen because of other predators
        for (_, predator_pos) in perceptions.nearby_predators:
            relative_pos = agent_pos.get_simple_relative_orientation(predator_pos)

            if relative_pos == GridRelativeOrientation.FRONT:
                probability_map.update_prob(MyAction.NORTH, -predator_potential)

            elif relative_pos == GridRelativeOrientation.FRONT_LEFT:
                probability_map.update_prob(MyAction.NORTH, -predator_potential)
                probability_map.update_prob(MyAction.WEST, -predator_potential)

            elif relative_pos == GridRelativeOrientation.FRONT_RIGHT:
                probability_map.update_prob(MyAction.NORTH, -predator_potential)
                probability_map.update_prob(MyAction.EAST, -predator_potential)

            elif relative_pos == GridRelativeOrientation.LEFT:
                probability_map.update_prob(MyAction.WEST, -predator_potential)

            elif relative_pos == GridRelativeOrientation.RIGHT:
                probability_map.update_prob(MyAction.EAST, -predator_potential)

            elif relative_pos == GridRelativeOrientation.BACK:
                probability_map.update_prob(MyAction.SOUTH, -predator_potential)

            elif relative_pos == GridRelativeOrientation.BACK_LEFT:
                probability_map.update_prob(MyAction.SOUTH, -predator_potential)
                probability_map.update_prob(MyAction.WEST, -predator_potential)

            elif relative_pos == GridRelativeOrientation.BACK_RIGHT:
                probability_map.update_prob(MyAction.SOUTH, -predator_potential)
                probability_map.update_prob(MyAction.EAST, -predator_potential)

        ## examine actions which are more likely to be chosen because of prey
        for (_, prey_pos) in perceptions.nearby_prey:
            relative_pos = agent_pos.get_simple_relative_orientation(prey_pos)

            if relative_pos == GridRelativeOrientation.FRONT:
                probability_map.update_prob(MyAction.NORTH, prey_potential)

            elif relative_pos == GridRelativeOrientation.FRONT_LEFT:
                probability_map.update_prob(MyAction.NORTH, prey_potential)
                probability_map.update_prob(MyAction.WEST, prey_potential)

            elif relative_pos == GridRelativeOrientation.FRONT_RIGHT:
                probability_map.update_prob(MyAction.NORTH, prey_potential)
                probability_map.update_prob(MyAction.EAST, prey_potential)

            elif relative_pos == GridRelativeOrientation.LEFT:
                probability_map.update_prob(MyAction.WEST, prey_potential)

            elif relative_pos == GridRelativeOrientation.RIGHT:
                probability_map.update_prob(MyAction.EAST, prey_potential)

            elif relative_pos == GridRelativeOrientation.BACK:
                probability_map.update_prob(MyAction.SOUTH, prey_potential)

            elif relative_pos == GridRelativeOrientation.BACK_LEFT:
                probability_map.update_prob(MyAction.SOUTH, prey_potential)
                probability_map.update_prob(MyAction.WEST, prey_potential)

            elif relative_pos == GridRelativeOrientation.BACK_RIGHT:
                probability_map.update_prob(MyAction.SOUTH, prey_potential)
                probability_map.update_prob(MyAction.EAST, prey_potential)

        probability_map.normalize_probs()

        probability_map.print_()

        if not probability_map.empty():
            return probability_map.choice()
        else:
            return available_moves.choice()
        '''


class MyEnvironment(HuntingEnvironment):
    """
    Your implementation of the environment in which cleaner agents work.
    """
    PREY_RANGE = 2
    PREDATOR_RANGE = 3

    def __init__(self, w, h, num_predators, num_prey):
        self.gathered = False
        self.scattered = False
        self.patroling = False
        self.target_row = h // 2 + 1
        self.target_col = w // 2 + 1
        self.target_cell = GridPosition(self.target_col, self.target_row)
        self.num_predators = num_predators
        self.w = w
        self.h = h
        self.patrol_target = {}
        """
        Default constructor. This should call the initialize methods offered by the super class.
        """
        rand_seed = 42
        # rand_seed =  datetime.now()

        print("Seed = %i" % rand_seed)

        super(MyEnvironment, self).__init__()

        predators = []
        prey = []

        for i in range(num_predators):
            new_predator = MyPredator()
            new_predator.set_gather_target(self.target_col, self.target_row)
            predators.append(new_predator)

        for i in range(num_prey):
            prey.append(MyPrey())

        """ Message box for messages that need to be delivered by the environment to their respective recepients, on
        the next turn """
        self.message_box = []

        ## initialize the huniting environment
        self.initialize(w=w, h=h, predator_agents=predators, prey_agents=prey, rand_seed = rand_seed)

    def gather(self):
        agent_perceptions = {}
        agent_actions = {}

        if not self.gathered:
            self.gathered = True

            # Move each agent towards the map center
            for predator_data in self._predator_agents:
                if not predator_data.grid_position == self.target_cell:
                    self.gathered = False
                else:
                    continue
                nearby_obstacles = self.get_nearby_obstacles(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
                agent_perceptions[predator_data] = MyAgentPerception(agent_position=predator_data.grid_position,
                                                                 obstacles=nearby_obstacles,
                                                                 nearby_predators=None,
                                                                 nearby_prey=None)
                agent_actions[predator_data] = predator_data.linked_agent.response(agent_perceptions[predator_data])
                #print("AgentActions[predator_data] = " + str(agent_actions[predator_data]))

            # Apply the actions for all agents which did not reach the center yet
            for predator_data in self._predator_agents:
                if predator_data.grid_position == self.target_cell:
                    continue
                predator_action = agent_actions[predator_data]
                new_position = predator_data.grid_position
                ## TODO: handle case for a SocialAction instance

                if isinstance(predator_action, SocialAction):
                    predator_action = predator_action.action
                    # TODO do something with the messages of this SocialAction

                if predator_action == MyAction.NORTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.NORTH)
                elif predator_action == MyAction.SOUTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.SOUTH)
                elif predator_action == MyAction.EAST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.EAST)
                elif predator_action == MyAction.WEST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.WEST)

                if not new_position in self._xtiles:
                    predator_data.grid_position = new_position
                    if new_position == GridPosition(self.target_col, self.target_row):
                        predator_data.linked_agent.gathered = True
                else:
                    print("Agent %s tried to go through a wall!" % str(predator_data))


    def scatter(self):
        stride_size_row = self.h // int(np.ceil(np.sqrt(self.num_predators)))
        stride_size_col = self.w // int(np.ceil(np.sqrt(self.num_predators)))

        for predator_data in self._predator_agents:
            nearby_obstacles = self.get_nearby_obstacles(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_predators = self.get_nearby_predators(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_prey = self.get_nearby_prey(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)

            predators = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_predators]
            prey = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_prey]

            for predator in predators:
                predator_data.linked_agent.known_predators.append(predator[0]) ## use ID only to identify predators

            patrol_row = (predator_data.linked_agent.id // int(np.ceil(np.sqrt(self.num_predators)))) * stride_size_row + 1 + stride_size_row // 2
            patrol_col = (predator_data.linked_agent.id % int(np.ceil(np.sqrt(self.num_predators)))) * stride_size_col + 1 + stride_size_col // 2
            print("ID: {} | NumPredators: {} | StrideSizeCol: {} | Res: {}".format(predator_data.linked_agent.id, self.num_predators, stride_size_col, (predator_data.linked_agent.id % self.num_predators)))
            self.patrol_target[predator_data.linked_agent.id] = (patrol_col, patrol_row)
            print("Agent {} going to {} | {}".format(predator_data.linked_agent.id, patrol_row, patrol_col))
            predator_data.linked_agent.set_patrol_target(patrol_col, patrol_row)

        self.scattered = True
        
    def patrol(self):
        agent_perceptions = {}
        agent_actions = {}

        if not self.patroling:
            self.patroling = True

            # Move each agent towards its patrol point
            for predator_data in self._predator_agents:
                if not predator_data.grid_position == GridPosition(*self.patrol_target[predator_data.linked_agent.id]):
                    self.patroling = False
                nearby_obstacles = self.get_nearby_obstacles(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
                agent_perceptions[predator_data] = MyAgentPerception(agent_position=predator_data.grid_position,
                                                                obstacles=nearby_obstacles,
                                                                nearby_predators=None,
                                                                nearby_prey=None)
                agent_actions[predator_data] = predator_data.linked_agent.response(agent_perceptions[predator_data])

            # Apply the actions for all agents which did not reach the patrol point yet
            for predator_data in self._predator_agents:
                if predator_data.grid_position == GridPosition(*self.patrol_target[predator_data.linked_agent.id]):
                    continue
                predator_action = agent_actions[predator_data]
                new_position = predator_data.grid_position

                if isinstance(predator_action, SocialAction):
                    predator_action = predator_action.action
                    # TODO do something with the messages of this SocialAction

                if predator_action == MyAction.NORTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.NORTH)
                elif predator_action == MyAction.SOUTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.SOUTH)
                elif predator_action == MyAction.EAST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.EAST)
                elif predator_action == MyAction.WEST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.WEST)

                if not new_position in self._xtiles:
                    predator_data.grid_position = new_position
                    if new_position == GridPosition(*self.patrol_target[predator_data.linked_agent.id]):
                        predator_data.linked_agent.patroling = True
                else:
                    print("Agent %s tried to go through a wall!" % str(predator_data))
            if self.patroling:
                MyPredator.ALL_ARE_PATROLING = True


    def step(self):
        """
        This method should iterate through all agents, provide them provide them with perceptions, and apply the
        action they return.
        """
        """
        STAGE 1: enerate perceptions for all agents, based on the state of the environment at the beginning of this
        turn
        """
        agent_perceptions = {}

        ## get perceptions for prey agents
        for prey_data in self._prey_agents:
            nearby_obstacles = self.get_nearby_obstacles(prey_data.grid_position, MyEnvironment.PREY_RANGE)
            nearby_predators = self.get_nearby_predators(prey_data.grid_position, MyEnvironment.PREY_RANGE)
            nearby_prey = self.get_nearby_prey(prey_data.grid_position, MyEnvironment.PREY_RANGE)

            predators = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_predators]
            prey = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_prey]

            agent_perceptions[prey_data] = MyAgentPerception(agent_position=prey_data.grid_position,
                                                             obstacles=nearby_obstacles,
                                                             nearby_predators=predators,
                                                             nearby_prey=prey)

        ## Each agent adds a message containing its current perceptions
        for predator_data in self._predator_agents:
            nearby_obstacles = self.get_nearby_obstacles(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_predators = self.get_nearby_predators(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_prey = self.get_nearby_prey(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)

            predators = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_predators]
            prey = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_prey]

            content = CustomMessage()
            for predator in predators:
                content.add_predators_pos(predator[0], predator[1])

            for prey_ in prey:
                content.add_prey_pos(prey_[0], prey_[1])

            for other_predator in predator_data.linked_agent.known_predators:
                self.message_box.append(AgentMessage(predator_data.linked_agent.id, other_predator, content))
            

        ## Create perceptions, along with the messages destined to the current predator
        x = 0
        for predator_data in self._predator_agents:
            x += 1
            nearby_obstacles = self.get_nearby_obstacles(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_predators = self.get_nearby_predators(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)
            nearby_prey = self.get_nearby_prey(predator_data.grid_position, MyEnvironment.PREDATOR_RANGE)

            predators = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_predators]
            prey = [(ag_data.linked_agent.id, ag_data.grid_position) for ag_data in nearby_prey]

            msgs = []
            for msg in self.message_box:
                if msg.destination_id == predator_data.linked_agent.id:
                    msgs.append(msg)

            agent_perceptions[predator_data] = MyAgentPerception(agent_position=predator_data.grid_position,
                                                             obstacles=nearby_obstacles,
                                                             nearby_predators=predators,
                                                             nearby_prey=prey,
                                                             messages=msgs)
        '''
        _process_message_box()

        for predator_data in self._predator_agents:
            msgs = []
            for msg in self.message_box:
                if msg.sender_id == predator_data.linked_agent.id:
                    #if not 
                if msg.destination_id == predator_data.linked_agent.id or msg.destination_id == -1:
                    msgs.append(msg)
            agent_perceptions[predator_data].messages = msgs

        self.message_box = []
        '''

            



        """
        STAGE 2: call response for each agent to obtain desired actions
        """
        agent_actions = {}
        ## TODO: get actions for all agents
        for prey_data in self._prey_agents:
            agent_actions[prey_data] = prey_data.linked_agent.response(agent_perceptions[prey_data])

        for predator_data in self._predator_agents:
            agent_actions[predator_data] = predator_data.linked_agent.response(agent_perceptions[predator_data])


        """
        STAGE 3: apply the agents' actions in the environment
        """
        for prey_data in self._prey_agents:
            if not prey_data in agent_actions:
                print("Agent %s did not opt for any action!" % str(prey_data))

            else:
                prey_action = agent_actions[prey_data]
                new_position = None

                if prey_action == MyAction.NORTH:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.NORTH)
                elif prey_action == MyAction.SOUTH:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.SOUTH)
                elif prey_action == MyAction.EAST:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.EAST)
                elif prey_action == MyAction.WEST:
                    new_position = prey_data.grid_position.get_neighbour_position(GridOrientation.WEST)

                if not new_position in self._xtiles:
                    prey_data.grid_position = new_position
                else:
                    print("Agent %s tried to go through a wall!" % str(prey_data))

        for predator_data in self._predator_agents:
            if not predator_data in agent_actions:
                print("Agent %s did not opt for any action!" % str(predator_data))

            else:
                predator_action = agent_actions[predator_data]
                new_position = predator_data.grid_position
                ## TODO: handle case for a SocialAction instance

                if isinstance(predator_action, SocialAction):
                    predator_action = predator_action.action
                    # TODO do something with the messages of this SocialAction

                if predator_action == MyAction.NORTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.NORTH)
                elif predator_action == MyAction.SOUTH:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.SOUTH)
                elif predator_action == MyAction.EAST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.EAST)
                elif predator_action == MyAction.WEST:
                    new_position = predator_data.grid_position.get_neighbour_position(GridOrientation.WEST)

                if not new_position in self._xtiles:
                    predator_data.grid_position = new_position
                else:
                    print("Agent %s tried to go through a wall!" % str(predator_data))

        self.message_box = []

        """
        At the end of the turn remove the dead prey
        """
        # If predators are moving towards a dead prey, notify them such that they change target
        for prey_data in self._prey_agents:
            if self.is_dead_prey(prey_data):
                for predator_data in self._predator_agents:
                    predator_data.linked_agent.remove_target(prey_data)
        self.remove_dead_prey()

    def gather_scatter_or_step(self):
        if not self.gathered:
            self.gather()
        elif not self.scattered:
            self.scatter()
        elif not self.patroling:
            self.patrol()
        else:
            self.step()

class Tester(object):

    NUM_PREDATORS = 8
    NUM_PREY = 7

    WIDTH = 25
    HEIGHT = 7

    DELAY = 0.1

    def __init__(self):
        self.env = MyEnvironment(Tester.WIDTH, Tester.HEIGHT, Tester.NUM_PREDATORS, Tester.NUM_PREY)
        self.make_steps()

    def make_steps(self):
        num_steps = 0
        while not self.env.goals_completed():
            #self.env.step()
            self.env.gather_scatter_or_step()
            num_steps += 1

            print(self.env)

            time.sleep(Tester.DELAY)

        print("Done in " + str(num_steps) + " steps")



if __name__ == "__main__":
    tester = Tester()
    #tester.make_steps()

