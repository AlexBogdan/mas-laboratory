from base import Action

class AgentMessage(object):
    def __init__(self, sender_id, destination_id, content):
        self.sender_id = sender_id
        self.destination_id = destination_id
        self.content = content

    @staticmethod
    def filter_messages_for(all_messages, agent):
        """
        helper method to filter from a set of messages only those for a specified agent.
        :param all_messages: the messages to filter.
        :param agent: the destination agent
        :return: messages for the specified destination
        """
        return [msg for msg in all_messages if msg.destination_id == agent.id]

    def print_(self):
        print("Sender: {} | Destination: {} | Content: {}".format(self.sender_id, self.destination_id, self.content))

class SocialAction(Action):
    def __init__(self, physical_action):
        """
        Initialize a social action
        :param physical_action: the physical action the agent wants to perform
        """
        self.action = physical_action

        ## the set of outgoing messages, initially empty
        self.outgoing_messages = []


    def add_outgoing_message(self, sender_id, destination_id, content):
        self.outgoing_messages.append(AgentMessage(sender_id, destination_id, content))

class CustomMessage(object):
    def __init__(self):
        self.targets = {}           ## maps a prey to the predators it is followed by
        self.prey_pos = {}          ## maps a prey (by id) to a flag telling whether it is followed or not
        self.predators_pos = {}     ## maps a predator (by id) to a flag telling whether it is following a prey or not

    def add_target(self, prey_id, predator_id):
        if not prey_id in self.targets:
            self.targets[prey_id] = [predator_id]
        else:
            self.targets[prey_id].append(predator_id)

    def add_prey_pos(self, prey_id, prey_pos):
        self.prey_pos[prey_id] = prey_pos

    def add_predators_pos(self, predator_id, predator_pos):
        self.predators_pos[predator_id] = predator_pos

    def __repr__(self):
        s = "\nTargets: "

        for (k, v) in self.targets.items():
            s += "<{} | {}> ".format(k, v)

        s += "\nPrey pos: "
        for (k, v) in self.prey_pos.items():
            s += "<{} | {}> ".format(k, v)

        s += "\nPredators pos: "
        for (k, v) in self.predators_pos.items():
            s += "<{} | {}> ".format(k, v)

        return s
