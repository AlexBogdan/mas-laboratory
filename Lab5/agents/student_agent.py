from typing import List, Dict, Any

from scipy.stats._multivariate import special_ortho_group_frozen

from agents import HouseOwnerAgent, CompanyAgent
from communication import NegotiationMessage


class MyACMEAgent(HouseOwnerAgent):

    def __init__(self, role: str, budget_list: List[Dict[str, Any]]):
        super(MyACMEAgent, self).__init__(role, budget_list)
        self.auction_fractions = [0.6, 0.75, 0.90]
        self.negotiation_fractions = [0.6, 0.75, 1]
        self.auction_prices = {}

    def propose_item_budget(self, auction_item: str, auction_round: int) -> float:
        # Proposing an ascending price each round
        self.auction_prices[auction_item] = self.budget_dict[auction_item] * self.auction_fractions[auction_round]
        return self.auction_prices[auction_item]

    def notify_auction_round_result(self, auction_item: str, auction_round: int, responding_agents: List[str]):
        pass

    def provide_negotiation_offer(self, negotiation_item: str, partner_agent: str, negotiation_round: int) -> float:
        # proposing again an ascending price till the established auction price of this item
        return self.auction_prices[negotiation_item] * self.negotiation_fractions[negotiation_round]

    def notify_negotiation_winner(self, negotiation_item: str, winning_agent: str, winning_offer: float) -> None:
        pass


class MyCompanyAgent(CompanyAgent):

    def __init__(self, role: str, specialties: List[Dict[str, Any]]):
        super(MyCompanyAgent, self).__init__(role, specialties)
        self.negotiation_fractions = [0.5, 0.65, 0.90]  # how our negotiation will be affected
        self.auction_prices = {}  # save the established auction price
        self.auction_members = {}  # save the number of companies that won the auction
        self.won_auction = False

    def decide_bid(self, auction_item: str, auction_round: int, item_budget: float) -> bool:
        min_budget = 0
        if auction_item in self.specialties:
            min_budget = self.specialties[auction_item]

        self.auction_prices[auction_item] = item_budget

        # We will bid only if the proposed price is bigger than our company minimum price
        return item_budget >= min_budget

    def notify_won_auction(self, auction_item: str, auction_round: int, num_selected: int):
        self.auction_members[auction_item] = num_selected
        self.won_auction = True

    def respond_to_offer(self, initiator_msg: NegotiationMessage) -> float:
        # print(initiator_msg)

        item = initiator_msg.negotiation_item
        round = initiator_msg.round

        # our company is the only one that is negotiating, so there's no need to lower the auction price
        if self.auction_members[item] == 1:
            return self.auction_prices[item] - (0.00001 * round)

        # just propose a smaller price than the established one after the bid
        return self.auction_prices[item] * self.negotiation_fractions[- (round + 1)]

    def notify_contract_assigned(self, construction_item: str, price: float) -> None:
        pass

    def notify_negotiation_lost(self, construction_item: str) -> None:
        pass
