Implementarea este destul de simplista (probabil mai mult a durat intelegerea codului)

    Initial Owner va propune 3 preturi pentru fiecare item un pret mai mic fata de cel maxim permis,
folosind procentele [0.6, 0.75, 0.90].
    Companiile care nu au obtinut niciun contract vor accepta primul pret propus pe care il gasesc mai mare
decat pretul lor minim pentru respectivul item.
    In cazul in care compania deja are o licitatie castigata, asteapta pana in ultima runda pentru a licita,
riscand astfel pentru un pret mai bun (avand deja o sansa sa castige primul contract)


    In cazul negocierilor ownerul doar va incerca sa modifice pretul stabilit in urma licitarii, exact ca in cazul
anterior, folosind valorile [0.6, 0.75, 1]
    Companiile in schimb vor folosi valorile [0.5, 0.65, 0.90] (parcurse invers) pentru a obtine un pret
apropiat de cel de licitatie.
    In cazul in care o companie este insa singura intr-o negociere (singura care a castigat licitatia), atunci
nu va scadea pretul deloc, nefiind un alt competitor care sa ofere o suma mai mica oricum.